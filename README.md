# gocd-plumber
Create gocd-pipelines from YAML config.

## Build
`go build`

## Dev
### GoCD
`git clone https://github.com/dennisgranath/gocd-docker.git` and run `docker-compose up`.

### Create example pipeline
`cd examples && ../gocd-plumber -config ../root/etc/gocd-plumber/config.ini`


